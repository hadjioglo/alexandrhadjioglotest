package Exc8;

public class Pufuleti {
    String name;
    public Pufuleti(){
    }

    public void create(String name){
        this.name = name;
        System.out.println(name + " is produced");
    }

    public void transfer(String name){
        System.out.println(name + " are in the car");
    }

    public void transferToStores(String name){
        System.out.println(name + " are in Stores");
    }
}
