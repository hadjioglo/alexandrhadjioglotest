package Exc8;

public class Main {
    public static void main(String[] args) {

        Pufuleti box3 = new Pufuleti();
        Pufuleti box4 = new Pufuleti();

        //Produce pufuleti
        box3.create("Cristinuta");
        box4.create("Cristinel");

        //transfer to car
        box3.transfer("Cristinuta");
        box4.transfer("Cristinel");

        //transfer to Chisinau stores
        box3.transferToStores("Cristinuta");
        box4.transferToStores("Cristinel");

    }


}
