public class Exc6 {
    public static void main(String[] args) {
        String text = "I love my job and want to become the best in it, and more";

        System.out.println("First appearence of letter 'm' " + text.indexOf("m"));
        System.out.println("Last appearence of letter 'm' " + text.lastIndexOf("m"));
    }
}
