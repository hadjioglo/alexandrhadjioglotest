package Exc13;

public class Transformer {

    //this variables are incapuslated and can be accessed only by the setter, getter
    private String name;
    public static int height;

    public Transformer(){
    }


//getters
    public String getName() {
        return name;
    }

    public int getHeight() {
        return height;
    }

//setters
    public void setName(String name) {
        this.name = name;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    //polimorphysm
    public void speed(){
        System.out.println("Super fast 1000 km/h");
    }

}
