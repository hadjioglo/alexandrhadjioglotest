package Exc13;

public class Main {
    public static void main(String[] args) {
        Transformer transformer = new Transformer();

        //incapsulation
        transformer.setName("Megatron");
        String name = transformer.getName();
        System.out.println(name);

        transformer.setHeight(3);
        int height = transformer.getHeight();
        System.out.println("height " + height);




    }

}
