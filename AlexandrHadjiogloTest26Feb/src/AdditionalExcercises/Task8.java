package AdditionalExcercises;

import java.util.Scanner;

public class Task8 {
    public static void main(String [] args){
        Scanner day = new Scanner(System.in);

        System.out.println("Enter time in 24h format ");
        int hours = day.nextInt();

        if(hours < 5){
            System.out.println("It's night");
        } else if (hours < 10 && hours >= 5 ){
            System.out.println("It's morning");
        } else if (hours < 15 && hours >= 10 ){
            System.out.println("It's day");
        } else if (hours < 21 && hours >= 15 ){
            System.out.println("It's evening");
        } else System.out.println("It's night");
    }
}
