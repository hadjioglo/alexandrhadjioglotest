package AdditionalExcercises;

import java.util.Scanner;

public class Task19 {
    public static void main(String[] args) {
        Scanner obj1 = new Scanner(System.in);
        String input = obj1.nextLine();

        switch (input){
            case "R":
                System.out.println("Red");
                break;
            case "Y":
                System.out.println("Yellow");
                break;
            default:
                System.out.println("No such color");
                break;
        }
    }
}
