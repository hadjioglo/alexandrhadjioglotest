package AdditionalExcercises;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input a text: ");
        String text = input.nextLine();

        System.out.println("String length is = " + text.length());
        System.out.println("Max index is " + (text.length() - 1));
    }
}
