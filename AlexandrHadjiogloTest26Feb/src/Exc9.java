public class Exc9 {
    public static void main(String[] args) {

        Exc9 obj1 = new Exc9();
        System.out.println("Perimeter of rectangular " + obj1.calculatePerimeter(8));
    }

    int perimeter = 0;
    public int calculatePerimeter(int x){ //method to calculate the perimeter
        perimeter = x*4;
        return perimeter;
    }
}
