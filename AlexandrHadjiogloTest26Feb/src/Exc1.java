public class Exc1 {
    public static void main(String[] args) {

        //convert to short
        byte x = 32;
        short y = (short) x;
        System.out.println("Short: " + y);

        //convert to double
        int x1 = 33;
        double x2 = (double) x1;
        System.out.println("Double: " + x2);

        //convert from long in int
        long y1 = 54354365;
        int y2 = (int) y1;
        System.out.println("int: " + y2);
    }
}
