import java.util.Scanner;

public class Exc4 {
    public static void main(String[] args) {
        Scanner obj1 = new Scanner(System.in);
        System.out.println("Input 3 numbers");
        int n1 = obj1.nextInt();
        int n2 = obj1.nextInt();
        int n3 = obj1.nextInt();

        if (n1 == n2 && n2 == n3){
            System.out.println("All three numbers are equal");
        } else if(n1 != n2 && n2 != n3 && n1!=n3){
            System.out.println("All three numbers are different");
        } else System.out.println("Numbers are not equal nor different");
    }
}
