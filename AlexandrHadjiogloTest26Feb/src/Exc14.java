import java.util.Scanner;

public class Exc14 {
    public static void main(String[] args) {

        try{
            int [] array = {1, 2};
            System.out.println(array[2]);
        } catch (Exception ex){
            System.out.println("The length of array is 2, and you've tried to print element that is out of array");
            }

    }
}
